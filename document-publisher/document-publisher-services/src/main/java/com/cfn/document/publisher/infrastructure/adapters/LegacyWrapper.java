
package com.cfn.document.publisher.infrastructure.adapters;

import com.cfn.document.publisher.base.exceptions.BusinessException;
import com.cfn.document.publisher.base.exceptions.SystemException;

public interface LegacyWrapper<I, O, R> {

    // [resource setup logic] -------------------------------------

    void initResources(R resource) throws SystemException;

    // [dispatching logic] -------------------------------------

    O callTarget(I input) throws SystemException, BusinessException;

}
