package com.cfn.document.publisher.integration.ws;

import com.cfn.document.publisher.base.exceptions.ExceptionBuilder;
import com.cfn.document.publisher.base.exceptions.HandlerException;
import com.cfn.document.publisher.base.exceptions.PublisherException;
import com.cfn.document.publisher.base.exceptions.SystemException;
import com.cfn.document.publisher.infrastructure.handlers.HandlerProcessor;
import com.cfn.document.publisher.infrastructure.handlers.context.ContextHolder;
import com.cfn.document.publisher.model.TemplateReportGenerationRequest;
import com.cfn.document.publisher.model.TemplateReportGenerationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;


@WebService(targetNamespace = "http://cfn.com/document-publisher/services")
public class PublisherWS {

    @Autowired
    @Qualifier("template_extractor")
    private HandlerProcessor processor;

    public PublisherWS() {
        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
    }

    @WebMethod(operationName = "publishDocument", action = "publishDocument")
    public TemplateReportGenerationResponse publishDocument(@WebParam(name = "templateReportGenerationRequest") TemplateReportGenerationRequest request) throws PublisherException {

        ContextHolder context = ContextHolder.newInstance();
        context.getParameters().put(ContextHolder.PAYLOAD, request);

        try {

            processor.handle(context);
            TemplateReportGenerationResponse response = (TemplateReportGenerationResponse) context.getParameters().get(ContextHolder.RESPONSE);
            return response;

        } catch (HandlerException he) {
            throw ExceptionBuilder.newBuilder()
                    .withMessage("")
                    .withRootException(he)
                    .buildPublisherException();
        }
    }

}
