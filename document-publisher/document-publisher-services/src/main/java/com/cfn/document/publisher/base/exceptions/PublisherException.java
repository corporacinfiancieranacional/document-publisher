package com.cfn.document.publisher.base.exceptions;

import javax.xml.ws.WebFault;

/**
 * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * CFN
 * <p>
 * Created: 8-Mar-2016
 * Author:jprodriguez
 * Type: JAVA class
 * Artifact Purpose: Exception - Model Artifact
 * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 */

@WebFault
public class PublisherException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private String reason;

    public PublisherException() {
        super();
    }

    public PublisherException(String s) {
        super(s);
        this.reason = s;
    }

    public PublisherException(String s, Throwable throwable) {
        super(s, throwable);
        this.reason = s;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

}
