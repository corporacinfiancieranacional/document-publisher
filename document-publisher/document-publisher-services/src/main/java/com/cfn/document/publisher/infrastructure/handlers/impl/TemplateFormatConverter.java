package com.cfn.document.publisher.infrastructure.handlers.impl;


import com.cfn.document.publisher.base.exceptions.HandlerException;
import com.cfn.document.publisher.infrastructure.handlers.HandlerProcessor;
import com.cfn.document.publisher.infrastructure.handlers.context.ContextHolder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class TemplateFormatConverter extends HandlerProcessor {

    private static Logger LOGGER = LogManager.getLogger(TemplateFormatConverter.class.getName());

    public TemplateFormatConverter(final HandlerProcessor successor) {
        super(successor);
    }

    @Override
    public void handle(final ContextHolder context) throws HandlerException {

        LOGGER.info("---- step - TemplateFormatConverter");
        successor.handle(context);
    }
}
