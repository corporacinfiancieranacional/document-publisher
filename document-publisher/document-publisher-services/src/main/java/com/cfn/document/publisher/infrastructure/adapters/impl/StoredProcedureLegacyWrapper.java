package com.cfn.document.publisher.infrastructure.adapters.impl;


import com.cfn.document.publisher.base.constants.DataBaseResource;
import com.cfn.document.publisher.base.exceptions.BusinessException;
import com.cfn.document.publisher.base.exceptions.ExceptionBuilder;
import com.cfn.document.publisher.base.exceptions.SystemException;
import com.cfn.document.publisher.infrastructure.adapters.LegacyWrapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import java.sql.SQLException;
import java.util.Map;
import java.util.Map.Entry;



public class StoredProcedureLegacyWrapper implements LegacyWrapper<Map<String, Object>, Map<String, Object>, JdbcTemplate> {

    //[fields] ---------------------------

    private JdbcTemplate template;
    private String procedureName;
    private static Logger LOGGER = LogManager.getLogger(StoredProcedureLegacyWrapper.class.getName());

    //[constructor] ---------------------------

    private StoredProcedureLegacyWrapper(final String procedureName) {
        this.procedureName = procedureName;
    }

    public static StoredProcedureLegacyWrapper newInstance(final DataBaseResource sp) {
        return new StoredProcedureLegacyWrapper(sp.name());
    }

    //[method] ---------------------------

    @Override
    public void initResources(JdbcTemplate resource) throws SystemException {
        template = resource;
    }

    //[method] ---------------------------

    @Override
    public Map<String, Object> callTarget(Map<String, Object> input) throws SystemException, BusinessException {

        MapSqlParameterSource parameters = new MapSqlParameterSource();

        for(Entry<String, Object> entry : input.entrySet()){
            parameters.addValue(entry.getKey(), entry.getValue());
        }

        SimpleJdbcCall caller = new SimpleJdbcCall(template);
        caller.withProcedureName(procedureName);

        try {
            return caller.execute(parameters);
        } catch (Exception e) {

            LOGGER.error(e.getMessage());

            if (e.getCause() instanceof SQLException) {
                int errorCode = ((SQLException) e.getCause()).getErrorCode();
                throw ExceptionBuilder.newBuilder()
                        .withRootException(e)
                        .withMessage("system.database.stored_procedure.error." + errorCode)
                        .buildBusinessException();
            } else {
                throw ExceptionBuilder.newBuilder()
                        .withRootException(e)
                        .withMessage("system.database.error")
                        .buildSystemException();
            }
        }
    }

}
