package com.cfn.document.publisher.base.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ResourceBundle;

/**
 * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * CFN
 * <p>
 * Created: 8-Mar-2016
 * Author:jprodriguez
 * Type: JAVA class
 * Artifact Purpose: Exception - Model Artifact
 * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 */
public class MessageUtil {

    private static Logger LOGGER = LogManager.getLogger(MessageUtil.class.getName());
    private ResourceBundle bundle;
    private static MessageUtil INSTANCE;

    // [constructor] ----------------------------

    private MessageUtil() {
        try {
            bundle = ResourceBundle.getBundle("messages");
        } catch (Exception e) {
            LOGGER.error("Message Util - a system error has occurred", e);
            throw new RuntimeException(e);
        }
    }

    // ----------------------------

    public static MessageUtil getInstance() {
        if (INSTANCE == null)
            INSTANCE = new MessageUtil();
        return INSTANCE;
    }


    // ----------------------------

    public String getMessage(String key) {
        return bundle.getString(key);
    }

}
