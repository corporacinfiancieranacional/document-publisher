package com.cfn.document.publisher.model;

import lombok.Data;

@Data
public class TemplateReportGenerationRequest {

    private String templateType;
    private String instanceNumber;
    private String fileNetDocumentId;
    private boolean convertToPdf;

}
