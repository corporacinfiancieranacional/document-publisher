package com.cfn.document.publisher.infrastructure.handlers.context;

import lombok.Data;

import java.util.HashMap;

@Data
public class ContextHolder {

    public static final String PAYLOAD = "PAYLOAD";
    public static final String RESPONSE = "RESPONSE";

    private HashMap<String, Object> parameters = new HashMap<>();

    public static ContextHolder newInstance() {
        return new ContextHolder();
    }

}
