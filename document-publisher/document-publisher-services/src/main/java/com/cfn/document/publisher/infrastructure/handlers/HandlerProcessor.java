package com.cfn.document.publisher.infrastructure.handlers;


import com.cfn.document.publisher.base.exceptions.HandlerException;
import com.cfn.document.publisher.infrastructure.handlers.context.ContextHolder;

public abstract class HandlerProcessor {

    protected final HandlerProcessor successor;

    protected HandlerProcessor(HandlerProcessor successor) {
        this.successor = successor;
    }

    public abstract void handle(ContextHolder context) throws HandlerException;

}
