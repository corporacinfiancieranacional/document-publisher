package com.cfn.document.publisher.base.exceptions;

import com.cfn.document.publisher.base.util.MessageUtil;

/**
 * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * CFN
 * <p>
 * Created: 8-Mar-2016
 * Author:jprodriguez
 * Type: JAVA class
 * Artifact Purpose: Exception - Model Artifact
 * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 */
public class ExceptionBuilder {

    private String message;
    private Throwable rootException;

    private ExceptionBuilder() {
    }

    public static ExceptionBuilder newBuilder() {
        return new ExceptionBuilder();
    }

    public ExceptionBuilder withMessage(String message) {
        this.message = MessageUtil.getInstance().getMessage(message);
        return this;
    }

    public ExceptionBuilder withRootException(Throwable rootException) {
        this.rootException = rootException;
        return this;
    }

    public BusinessException buildBusinessException() {
        return new BusinessException(message, rootException);
    }

    public SystemException buildSystemException() {
        return new SystemException(message, rootException);
    }

    public SystemException buildHandlerException() {
        return new SystemException(message, rootException);
    }

    public PublisherException buildPublisherException() {return new PublisherException(message, rootException);}

}
