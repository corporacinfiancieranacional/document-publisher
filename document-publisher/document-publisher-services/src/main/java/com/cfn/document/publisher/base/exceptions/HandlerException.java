package com.cfn.document.publisher.base.exceptions;

/**
 * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * CFN
 * <p>
 * Created: 8-Mar-2016
 * Author:jprodriguez
 * Type: JAVA class
 * Artifact Purpose: Exception - Model Artifact
 * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 */

public class HandlerException extends Exception {

    private static final long serialVersionUID = 1L;
    private String reason;

    public HandlerException() {
        super();
    }

    public HandlerException(String s) {
        super(s);
        this.reason = s;
    }

    public HandlerException(String s, Throwable throwable) {
        super(s, throwable);
        this.reason = s;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
