package com.cfn.document.publisher.infrastructure.adapters.impl;

import com.cfn.document.publisher.base.exceptions.BusinessException;
import com.cfn.document.publisher.base.exceptions.SystemException;
import com.cfn.document.publisher.infrastructure.adapters.LegacyWrapper;

/**
 * Created by jrodriguez on 08/03/2017.
 */
public class FileNetLegacyWrapper implements LegacyWrapper<String, String, String> {

    @Override
    public void initResources(String resource) throws SystemException {

    }

    @Override
    public String callTarget(String input) throws SystemException, BusinessException {
        return null;
    }
}
