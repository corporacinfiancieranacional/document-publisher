package com.cfn.document.publisher.infrastructure.handlers.impl;


import com.cfn.document.publisher.base.constants.DataBaseResource;
import com.cfn.document.publisher.base.exceptions.BusinessException;
import com.cfn.document.publisher.base.exceptions.HandlerException;
import com.cfn.document.publisher.base.exceptions.SystemException;
import com.cfn.document.publisher.infrastructure.adapters.impl.DBFunctionLegacyWrapper;
import com.cfn.document.publisher.infrastructure.handlers.HandlerProcessor;
import com.cfn.document.publisher.infrastructure.handlers.context.ContextHolder;
import com.cfn.document.publisher.model.TemplateReportGenerationRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.*;


public class TemplateFiller extends HandlerProcessor {

    private static Logger LOGGER = LogManager.getLogger(TemplateFiller.class.getName());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public TemplateFiller(final HandlerProcessor successor) {
        super(successor);
    }

    @Override
    public void handle(final ContextHolder context) throws HandlerException {

        LOGGER.info("---- step - TemplateFiller");

        TemplateReportGenerationRequest request = (TemplateReportGenerationRequest) context.getParameters().get(ContextHolder.PAYLOAD);

        try {
            DBFunctionLegacyWrapper legacyWrapper = DBFunctionLegacyWrapper.newInstance(DataBaseResource.CONSULTAR_ARCHIVOS);
            legacyWrapper.initResources(jdbcTemplate);
            List<Map<String, Object>> response = legacyWrapper.callTarget(Collections.EMPTY_MAP);

            for (Map<String, Object> map :response){
                for(Map.Entry <String, Object> entry :map.entrySet()){
                    LOGGER.info(entry.getKey() +"--"+entry.getValue());
                }

            }

            //TODO: complete logic paramert template
            successor.handle(context);

        } catch (SystemException | BusinessException e) {
            //TODO: do log
            throw new HandlerException(e.getMessage(),e);
        }

    }
}
