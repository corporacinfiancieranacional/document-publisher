package com.cfn.document.publisher.base.builder.impl;

import com.cfn.document.publisher.base.builder.Builder;
import com.cfn.document.publisher.model.TemplateReportGenerationResponse;

/**
 * Created by jrodriguez on 12/03/2017.
 */
public class TemplateReportGenerationResponseBuilder implements Builder<TemplateReportGenerationResponse> {

    private String code;
    private String description;
    private String fileNetDocumentId;

    private TemplateReportGenerationResponseBuilder() {}

    public static TemplateReportGenerationResponseBuilder newInstance() {
        return new TemplateReportGenerationResponseBuilder();
    }

    public TemplateReportGenerationResponseBuilder withCode(final String code) {
        this.code = code;
        return this;
    }

    public TemplateReportGenerationResponseBuilder withDescription(final String description) {
        this.description = description;
        return this;
    }

    public TemplateReportGenerationResponseBuilder withFileNetDocumentId(final String fileNetDocumentId) {
        this.fileNetDocumentId = fileNetDocumentId;
        return this;
    }

    @Override
    public TemplateReportGenerationResponse build() {
        return new TemplateReportGenerationResponse(code, description, fileNetDocumentId);
    }
}
