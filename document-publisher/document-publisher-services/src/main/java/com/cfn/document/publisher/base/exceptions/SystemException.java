package com.cfn.document.publisher.base.exceptions;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 * CFN
 * <p>
 * Created: 31-Dic-2016
 * Author:jprodriguez
 * Type: JAVA class
 * Artifact Purpose: Exception - Model Artifact
 * ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 */
public class SystemException extends Exception {

    private static final long serialVersionUID = 1L;
    private String reason;

    public SystemException() {
        super();
    }

    public SystemException(String s) {
        super(s);
        this.reason = s;
    }

    public SystemException(String s, Throwable throwable) {
        super(s, throwable);
        this.reason = s;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

}
