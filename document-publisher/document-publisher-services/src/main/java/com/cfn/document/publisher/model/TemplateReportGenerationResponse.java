package com.cfn.document.publisher.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TemplateReportGenerationResponse {

    private String code;
    private String description;
    private String fileNetDocumentId;

    public TemplateReportGenerationResponse(String code, String description, String fileNetDocumentId) {
        this.code = code;
        this.description = description;
        this.fileNetDocumentId = fileNetDocumentId;
    }
}
