package com.cfn.document.publisher.infrastructure.handlers.impl;

import com.cfn.document.publisher.base.builder.impl.TemplateReportGenerationResponseBuilder;
import com.cfn.document.publisher.base.exceptions.HandlerException;
import com.cfn.document.publisher.infrastructure.handlers.HandlerProcessor;
import com.cfn.document.publisher.infrastructure.handlers.context.ContextHolder;
import com.cfn.document.publisher.model.TemplateReportGenerationResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class DocumentPublisher extends HandlerProcessor {

    private static Logger LOGGER = LogManager.getLogger(DocumentPublisher.class.getName());


    public DocumentPublisher(final HandlerProcessor successor) {
        super(successor);
    }

    @Override
    public void handle(final ContextHolder context) throws HandlerException {

        LOGGER.info("---- step - DocumentPublisher");
        TemplateReportGenerationResponse response = TemplateReportGenerationResponseBuilder.newInstance().withCode("334543")
                .withDescription("Exito")
                .withFileNetDocumentId("50098-SD")
                .build();

        context.getParameters().put(ContextHolder.RESPONSE,response);
    }
}
